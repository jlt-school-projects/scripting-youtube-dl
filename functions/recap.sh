function recap()
{
	echo "|############################|"
	echo "|#RECAPITULATIF DU WORKSPACE#|"
	echo "|############################|"
	if [ -f success_dl.log ]
	then
		echo "$(wc -l < success_dl.log) vidéos téléchargées"
	fi
	echo "$(ls | grep tar.gz | wc -l) archive(s) de vidéos"
	echo "$(du -chs | tail -1 | cut -f -1) used for project"
	echo "$(du -chs *.tar.gz | tail -1 | cut -f -1) used for archives"
}
