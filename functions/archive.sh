function archive()
{
	mkdir -p .toarchive
	for video in $(ls videos | grep -v tar.)
	do
		maj=${video^^}
		first=${maj:0:1}
		echo $first
		mkdir -p .toarchive/$first
		mv videos/$video .toarchive/$first/$video
	done
        tar cvf $(date +%d-%m-%Y-%H-%M-%S).tar.gz .toarchive
	rm -rf .toarchive

}
